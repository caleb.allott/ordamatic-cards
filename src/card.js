import React from 'react';
import s from './card.module.css';

const Card = props => {
  const { buyer, shipped, dueDate, orderNumber, items, invoice, courier, reference, notes } = props;
  return (
    <div className={s.card}>
      <header className={s.header}>
        <h3 className={s.buyer}>
          {buyer} <span className={s.orderNumber}>{orderNumber}</span>
        </h3>
        {shipped && <p className={s.status}>Shipped</p>}
      </header>
      <p className={s.detailsSm}>
        {items} items, due {dueDate}
      </p>
      <dl className={s.detailsLg}>
        <div className={s.dueDate}>
          <dt>Due</dt>
          <dd>{dueDate}</dd>
        </div>
        <div className={s.items}>
          <dt>Items</dt>
          <dd>{items}</dd>
        </div>
        <div className={s.invoice}>
          <dt>Invoice</dt>
          <dd>{invoice}</dd>
        </div>
        {courier && (
          <div className={s.courier}>
            <dt>Courier</dt>
            <dd>{courier}</dd>
          </div>
        )}
        {reference && (
          <div className={s.reference}>
            <dt>Reference</dt>
            <dd>{reference}</dd>
          </div>
        )}
        {notes && (
          <div className={s.notes}>
            <dt>Notes</dt>
            <dd>{notes}</dd>
          </div>
        )}
      </dl>
      <button className={s.menu}>
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          <g>
            <circle cx="6" cy="12" r="2" />
            <circle cx="12" cy="12" r="2" />
            <circle cx="18" cy="12" r="2" />
          </g>
        </svg>
      </button>
    </div>
  );
};

export default Card;
