import React from 'react';
import Card from './card.js';

function App() {
  return (
    <div>
      <Card
        buyer="Mojo Pantry"
        shipped
        dueDate="today"
        orderNumber="#0028"
        items="24"
        invoice="INV-1049"
        courier="PBT 8529038"
        reference="PO-0329"
        notes="Example of a long note lorem ipsum dolor sit amet consectetur adipiscing elit."
      />
      <Card
        buyer="Mojo Pantry"
        shipped
        dueDate="02 Nov"
        orderNumber="#0028"
        items="24"
        invoice="INV-1049"
        reference="PO-0329"
        notes="Example of no courier"
      />
      <Card
        buyer="Mojo Pantry"
        dueDate="02 Nov"
        orderNumber="#0028"
        items="24"
        invoice="INV-1049"
        courier="NZ8423958925433 AS3929"
        reference="PO-0329"
        notes="Long courier number"
      />
      <Card
        buyer="Mojo Pantry"
        dueDate="01 Nov"
        orderNumber="#0028"
        items="24"
        invoice="INV-1049"
        courier="PBT 8529038"
        reference="Example of a long reference field"
        notes="Long reference"
      />
      <Card
        buyer="Example of long long long long buyer name"
        dueDate="01 Nov"
        orderNumber="#0028"
        items="24"
        invoice="INV-1049"
      />
    </div>
  );
}

export default App;
